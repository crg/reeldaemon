#ifndef REELDAEMON_TCP_LISTENER_HPP
#define REELDAEMON_TCP_LISTENER_HPP

#include <functional>
#include <string>
#include <gio/gio.h>


namespace reeldaemon
{


class tcp_listener
{
public:
	typedef std::function < bool(std::string const &p_line) > line_callback;

	explicit tcp_listener(unsigned int p_port, line_callback const &p_line_callback);
	~tcp_listener();


private:
	enum { block_size = 4096 };

	struct connection_data
	{
		GSocketConnection *m_connection;
		std::array < char, block_size > m_data;
		tcp_listener *m_this;
		std::string m_cur_line;
	};

	static gboolean static_incoming_connection_callback(GSocketService *p_service, GSocketConnection *p_connection, GObject *p_source_object, gpointer p_user_data);
	gboolean incoming_connection_callback(GSocketService *p_service, GSocketConnection *p_connection, GObject *p_source_object);

	static void static_message_ready_callback(GObject *p_source_object, GAsyncResult *p_result, gpointer p_user_data);
	void message_ready_callback(GObject *p_source_object, GAsyncResult *p_result, connection_data *p_conn_data);


	GSocketService *m_service;
	GCancellable *m_cancellable;
	line_callback m_line_callback;
};


} // namespace reeldaemon end


#endif
