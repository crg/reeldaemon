#ifndef REELDAEMON_TOKENIZER_HPP
#define REELDAEMON_TOKENIZER_HPP

#include <vector>
#include <string>


namespace reeldaemon
{


typedef std::vector < std::string > tokens;

tokens tokenize_line(std::string const &p_line);


} // namespace reeldaemon end


#endif
