#include <algorithm>
#include "log.hpp"
#include "tcp_listener.hpp"


namespace reeldaemon
{


tcp_listener::tcp_listener(unsigned int p_port, line_callback const &p_line_callback)
	: m_line_callback(p_line_callback)
{
	m_cancellable = g_cancellable_new();
	m_service = g_socket_service_new();
	g_socket_listener_add_inet_port(G_SOCKET_LISTENER(m_service), p_port, NULL, NULL);
	g_signal_connect(m_service, "incoming", G_CALLBACK(static_incoming_connection_callback), this);

	LOG_MSG(debug, "listening to TCP connections at port " << p_port);
}


tcp_listener::~tcp_listener()
{
	g_cancellable_cancel(m_cancellable);
	g_socket_service_stop(m_service);
	g_object_unref(G_OBJECT(m_service));
	g_object_unref(G_OBJECT(m_cancellable));
}


gboolean tcp_listener::static_incoming_connection_callback(GSocketService *p_service, GSocketConnection *p_connection, GObject *p_source_object, gpointer p_user_data)
{
	return static_cast < tcp_listener* > (p_user_data)->incoming_connection_callback(p_service, p_connection, p_source_object);
}


gboolean tcp_listener::incoming_connection_callback(GSocketService *, GSocketConnection *p_connection, GObject *)
{
	GInputStream *gistream = g_io_stream_get_input_stream(G_IO_STREAM(p_connection));
	connection_data *conn_data = new connection_data;
	conn_data->m_connection = G_SOCKET_CONNECTION(g_object_ref(G_OBJECT(p_connection)));
	conn_data->m_this = this;

	g_input_stream_read_async(
		gistream,
		conn_data->m_data.data(), conn_data->m_data.size(),
		G_PRIORITY_DEFAULT,
		NULL,
		static_message_ready_callback,
		conn_data
	);

	LOG_MSG(debug, "reading from new incoming connection");

	return FALSE;
}


void tcp_listener::static_message_ready_callback(GObject *p_source_object, GAsyncResult *p_result, gpointer p_user_data)
{
	connection_data *conn_data = static_cast < connection_data* > (p_user_data);
	conn_data->m_this->message_ready_callback(p_source_object, p_result, conn_data);
}


void tcp_listener::message_ready_callback(GObject *p_source_object, GAsyncResult *p_result, connection_data *p_conn_data)
{
	GError *err = NULL;
	GInputStream *gistream = G_INPUT_STREAM(p_source_object);
	int count;
	bool finish_connection = FALSE;

	count = g_input_stream_read_finish(gistream, p_result, &err);

	if (count == 0)
	{
		finish_connection = TRUE;
	}
	else if (count != -1)
	{
		auto iter = p_conn_data->m_data.begin();
		auto max_iter = p_conn_data->m_data.begin() + count;
		while ((iter != max_iter) && (iter != p_conn_data->m_data.end()))
		{
			auto delimiter_iter = std::find(iter, max_iter, '\n');
			if (delimiter_iter == p_conn_data->m_data.end())
			{
				p_conn_data->m_cur_line += std::string(&(*iter), count);
				break;
			}
			else
			{
				p_conn_data->m_cur_line.append(&(*iter), delimiter_iter - iter);
				LOG_MSG(trace, "line: [" << p_conn_data->m_cur_line << "]");
				if (m_line_callback)
					finish_connection = !(m_line_callback(p_conn_data->m_cur_line));
				p_conn_data->m_cur_line = "";
			}
			iter = delimiter_iter + 1;
		}
	}
	else
	{
		g_assert(err != NULL);
		LOG_MSG(error, "could not finish async reading from network connection: " << err->message);
		g_error_free(err);
		finish_connection = TRUE;
	}

	if (finish_connection)
	{
		g_object_unref (G_SOCKET_CONNECTION(p_conn_data->m_connection));
		delete p_conn_data;
		LOG_MSG(debug, "terminated connection");
	}
	else
	{
		g_input_stream_read_async(
			gistream,
			p_conn_data->m_data.data(), p_conn_data->m_data.size(),
			G_PRIORITY_DEFAULT,
			m_cancellable,
			static_message_ready_callback,
			p_conn_data
		);
	}
}


} // namespace reeldaemon end
