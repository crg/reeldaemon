#ifndef REELDAEMON_SCOPE_GUARD_HPP
#define REELDAEMON_SCOPE_GUARD_HPP


namespace reeldaemon
{


template < typename RollbackFunc >
class scope_guard
{
public:
	typedef scope_guard < RollbackFunc > self;


	explicit scope_guard(RollbackFunc const &p_rollback_func)
		: m_rollback_func(p_rollback_func)
		, m_guarded(true)
	{
	}

	~scope_guard()
	{
		if (m_guarded)
			m_rollback_func();
	}

	scope_guard(self &&p_other)
		: m_rollback_func(std::move(p_other.m_rollback_func))
		, m_guarded(p_other.m_guarded)
	{
		p_other.m_guarded = false;
	}

	self& operator = (self &&p_other)
	{
		m_rollback_func = std::move(p_other.m_rollback_func);
		m_guarded = p_other.m_guarded;
		p_other.m_guarded = false;
		return *this;
	}


	void unguard()
	{
		m_guarded = false;
	}


	scope_guard(self const &) = delete;
	self& operator = (self const &) = delete;


private:
	RollbackFunc m_rollback_func;
	bool m_guarded;
};


template < typename RollbackFunc >
scope_guard < RollbackFunc > make_scope_guard(RollbackFunc const &p_rollback_func)
{
	return scope_guard < RollbackFunc > (p_rollback_func);
}


} // namespace reeldaemon end


#endif
