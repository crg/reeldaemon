#include <stdio.h>
#include <unistd.h>
#include <glib.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "cmdline_iface.hpp"


namespace reeldaemon
{


namespace
{

// Unfortunately, this global variable is necessary for GNU readline,
// since it does not allow to pass data pointers to callbacks
cmdline_iface *cmdline_iface_instance = nullptr;

}


cmdline_iface::cmdline_iface(std::string const &p_prompt, line_callback const &p_line_callback)
	: m_stdin_channel(nullptr)
	, m_line_callback(p_line_callback)
{
	g_assert(!p_prompt.empty());
	g_assert(p_line_callback);

	cmdline_iface_instance = this;
	m_line_callback = p_line_callback;

	// Make a backup of the current terminal settings
	tcgetattr(STDIN_FILENO, &m_term);

	// Set up GIO channel for stdin
	m_stdin_channel = g_io_channel_unix_new(0);
	g_io_add_watch(m_stdin_channel, G_IO_IN, &static_input_handler_callback, this);

	// Initialize the GNU readline callback based interface
	rl_callback_handler_install(p_prompt.c_str(), static_process_line_callback);
}


cmdline_iface::~cmdline_iface()
{
	// IO channel is _not_ shut down, just unref'd
	// shutting down the stdin channel would close() the stdin FD,
	// which usually isn't desirable (here, it would cause the
	// tcsetattr() call to fail)
	g_io_channel_unref(m_stdin_channel);
	cmdline_iface_instance = NULL;

	// Restore terminal settings as they were before the cmdline interface was set up
	// Sometimes, GNU readline screws up the terminal settings like ECHO or canonical mode
	// To counter this, the original settings are restored
	tcsetattr(STDIN_FILENO, TCSANOW, &m_term);
}


gboolean cmdline_iface::static_input_handler_callback(GIOChannel *, GIOCondition, gpointer)
{
	// Read one character from stdin
	// (at this point it is safe to do so without getting blocked)
	rl_callback_read_char();
	return TRUE;
}


void cmdline_iface::static_process_line_callback(char *p_line)
{
	cmdline_iface_instance->process_line_callback(p_line);
}


void cmdline_iface::process_line_callback(char *p_line)
{
	if (p_line != nullptr)
		m_line_callback(p_line);
}


} // namespace reeldaemon end
