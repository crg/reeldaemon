#ifndef REELDAEMON_CMDLINE_IFACE_HPP
#define REELDAEMON_CMDLINE_IFACE_HPP

#include <glib.h>
#include <termios.h>
#include <functional>
#include <string>


namespace reeldaemon
{


class cmdline_iface
{
public:
	typedef std::function < bool(std::string const &p_line) > line_callback;

	explicit cmdline_iface(std::string const &p_prompt, line_callback const &p_line_callback);
	~cmdline_iface();


private:
	static gboolean static_input_handler_callback(GIOChannel *p_io_channel, GIOCondition p_io_condition, gpointer p_user_data);
	static void static_process_line_callback(char *p_line);
	void process_line_callback(char *p_line);

	GIOChannel *m_stdin_channel;
	line_callback m_line_callback;
	struct termios m_term;
};


} // namespace reeldaemon end


#endif
