#ifndef REELDAEMON_SYSTEM_HPP
#define REELDAEMON_SYSTEM_HPP

#include <glib.h>
#include <memory>

#include "pipeline.hpp"
#include "log.hpp"
#include "tcp_listener.hpp"
#include "tokenizer.hpp"
#include "cmdline_iface.hpp"


namespace reeldaemon
{


class system
{
public:
	typedef std::unique_ptr < cmdline_iface > cmdline_iface_uptr;
	typedef std::unique_ptr < tcp_listener > tcp_listener_uptr;
	typedef std::unique_ptr < pipeline > pipeline_uptr;


	explicit system(bool const p_interactive_mode, int const p_port);
	~system();

	void run();
	void play(std::string const &p_uri);
	void set_next(std::string const &p_uri);


private:
	bool process_command(tokens p_tokens);


	bool m_interactive_mode;
	GMainLoop *m_loop;
	pipeline_uptr m_pipeline;
	tcp_listener_uptr m_tcp_listener;
	cmdline_iface_uptr m_cmdline_iface;
};


} // namespace reeldaemon end


#endif
