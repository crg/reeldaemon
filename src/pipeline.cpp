#include "log.hpp"
#include "media.hpp"
#include "pipeline.hpp"
#include "scope_guard.hpp"


namespace reeldaemon
{


std::string get_state_name(state const p_state)
{
	switch (p_state)
	{
		case state_idle: return "idle";
		case state_starting: return "starting";
		case state_resetting: return "resetting";
		case state_stopping: return "stopping";
		case state_playing: return "playing";
		case state_paused: return "paused";
		default: return "<invalid>";
	}
}


namespace
{



std::string message_to_str(GstMessage *p_msg, GstDebugLevel const p_level)
{
	// NOTE: even though this might be used for info and warnings as well,
	// it is still using GError, which can sound confusing
	GError *error = NULL;
	gchar *debug_info = NULL;

	switch (p_level)
	{
		case GST_LEVEL_INFO: gst_message_parse_info(p_msg, &error, &debug_info); break;
		case GST_LEVEL_WARNING: gst_message_parse_warning(p_msg, &error, &debug_info); break;
		case GST_LEVEL_ERROR: gst_message_parse_error(p_msg, &error, &debug_info); break;
		default: return std::string("<invalid level ") + gst_debug_level_get_name(p_level) + ">";
	}

	std::string text = error->message;
	if (debug_info != nullptr)
		text += std::string(" (debug info: ") + debug_info + ")";

	g_error_free(error);
	g_free(debug_info);

	return text;
}


template < typename T >
void checked_unref(T* &p_object)
{
	if (p_object != nullptr)
	{
		gst_object_unref(GST_OBJECT(p_object));
		p_object = nullptr;
	
	}
}


} // unnamed namespace end




stream::stream()
	: m_uridecodebin(nullptr)
	, m_injecteos(nullptr)
	, m_concat_sinkpad(nullptr)
	, m_concat(nullptr)
	, m_bin(nullptr)
{
}


stream::stream(GstElement *p_concat, GstBin *p_bin)
	: m_uridecodebin(nullptr)
	, m_injecteos(nullptr)
	, m_concat_sinkpad(nullptr)
	, m_concat(p_concat)
	, m_bin(p_bin)
{
}


stream::~stream()
{
	if (m_bin == nullptr)
		return;

	gst_element_set_state(m_uridecodebin, GST_STATE_NULL);
	gst_element_set_state(m_injecteos, GST_STATE_NULL);
	GstPad *injecteos_srcpad = gst_element_get_static_pad(m_injecteos, "src");
	gst_pad_unlink(injecteos_srcpad, m_concat_sinkpad);
	gst_object_unref(GST_OBJECT(injecteos_srcpad));
	gst_element_release_request_pad(m_concat, m_concat_sinkpad);
	gst_object_unref(GST_OBJECT(m_concat_sinkpad));

	gst_bin_remove_many(m_bin, m_uridecodebin, m_injecteos, NULL);

	m_bin = nullptr;
}


pipeline::pipeline()
	: m_current_state(state_idle)
	, m_current_gstreamer_state(GST_STATE_NULL)
	, m_pending_gstreamer_state(GST_STATE_VOID_PENDING)
	, m_pipeline(nullptr)
	, m_concat(nullptr)
	, m_video_sink(nullptr)
	, m_clock(nullptr)
	, m_duration(GST_CLOCK_TIME_NONE)
	, m_postponed_stop(false)
{
	auto elems_guard = make_scope_guard([&]()
	{
		checked_unref(m_pipeline);
		checked_unref(m_concat);
		checked_unref(m_video_sink);
		checked_unref(m_clock);
	});

	m_pipeline = gst_pipeline_new(nullptr);
	m_concat = gst_element_factory_make("concat2", "concat");
	m_video_sink = gst_element_factory_make("autovideosink", "video-sink");
	m_clock = gst_system_clock_obtain();

	if (m_pipeline == nullptr)
		throw std::runtime_error("could not create playbin");

	if (m_concat == nullptr)
		throw std::runtime_error("could not create concat element");

	if (m_video_sink == nullptr)
		throw std::runtime_error("could not create video sink");

	if (m_clock == nullptr)
		throw std::runtime_error("could not obtain system clock");

	elems_guard.unguard();

	gst_bin_add_many(GST_BIN(m_pipeline), m_concat, m_video_sink, nullptr);
	gst_element_link(m_concat, m_video_sink);

	g_signal_connect(m_concat, "about-to-switch-pad", G_CALLBACK(static_about_to_switch_pad), this);

	g_object_set(G_OBJECT(m_clock), "clock-type", GST_CLOCK_TYPE_REALTIME, NULL);
	gst_pipeline_use_clock(GST_PIPELINE(m_pipeline), m_clock);

	GstBus *bus = gst_pipeline_get_bus(GST_PIPELINE(m_pipeline));
	gst_bus_add_watch(bus, static_bus_watch, this);
	gst_object_unref(GST_OBJECT(bus));
}


pipeline::~pipeline()
{
	if (m_pipeline != nullptr)
	{
		set_pipeline_to_idle();
		// Free any existing streams here directly,
		// before the pipeline is torn down
		m_next_stream.reset();
		m_current_stream.reset();
		gst_object_unref(GST_OBJECT(m_pipeline));
	}

	if (m_clock != nullptr)
		gst_object_unref(GST_OBJECT(m_clock));
}


bool pipeline::play_media(media const &p_media, bool const p_play_now)
{
	std::lock_guard < std::mutex > lock(m_mutex);

	if ((m_current_state == state_idle) || p_play_now)
	{
		if (!is_valid(p_media))
			return false;

		if (is_transitioning())
		{
			LOG_MSG(debug, "postponing playback of media with URI " << p_media.m_uri);
			m_postponed_stop = false;
			m_postponed_media = p_media;
			return true;
		}

		LOG_MSG(debug, "playing media with URI " << p_media.m_uri << " now");

		m_duration = -1;

		m_next_stream.reset();
		m_current_stream.reset();
		m_current_stream = setup_stream(p_media);

		m_postponed_media = media();
		m_postponed_stop = false;

		switch (m_current_state)
		{
			case state_idle:
				m_current_state = state_starting;
				if (!set_gstreamer_state(GST_STATE_PLAYING))
				{
					LOG_MSG(error, "could not switch GStreamer pipeline to PLAYING ; stopping");
					set_pipeline_to_idle();
					return false;
				}
				break;
			case state_paused:
			case state_playing:
				update_cur_pad_offset();
				break;
			default:
				break;
		}
	}
	else
	{
		LOG_MSG(debug, "playing media with URI " << p_media.m_uri << " next");
		m_next_stream.reset();
		m_next_stream = setup_stream(p_media);

		if (m_current_stream->m_media.m_stop_time != GST_CLOCK_TIME_NONE)
		{
			if ((m_next_stream->m_media.m_start_time != GST_CLOCK_TIME_NONE) && (m_next_stream->m_media.m_start_time < m_current_stream->m_media.m_stop_time))
			{
				LOG_MSG(debug, "next media starts before current one stops - setting stop time to next media's start time");
				g_object_set(G_OBJECT(m_current_stream->m_injecteos), "eos-time", m_next_stream->m_media.m_start_time, nullptr);
			}
		}
		else
		{
			LOG_MSG(debug, "current media stop time undefined - setting stop time to next media's start time");
			g_object_set(G_OBJECT(m_current_stream->m_injecteos), "eos-time", m_next_stream->m_media.m_start_time, nullptr);
		}
	}

	return true;
}


void pipeline::stop()
{
	if ((m_pipeline == nullptr) || (m_current_state == state_stopping) || (m_current_state == state_idle))
		return;

	if (is_transitioning())
	{
		m_postponed_stop = true;
		m_postponed_media = media();
	}
	else
		set_pipeline_to_idle();
}


gint64 pipeline::get_current_position() const
{
	if ((m_pipeline == nullptr) || (m_current_state == state_idle))
		return gint64(GST_CLOCK_TIME_NONE);

        gint64 position;
        gboolean success = gst_element_query_position(GST_ELEMENT(m_pipeline), GST_FORMAT_TIME, &position);
        return success ? position : gint64(GST_CLOCK_TIME_NONE);
}


GstClockTime pipeline::get_current_time() const
{
	return gst_clock_get_time(m_clock);
}


GstClockTime pipeline::get_duration() const
{
	return m_duration;
}


void pipeline::update_duration()
{
	if ((m_pipeline != NULL) && (m_current_state != state_idle))
	{
		gint64 duration;
		gboolean success = gst_element_query_duration(GST_ELEMENT(m_pipeline), GST_FORMAT_TIME, &duration);
		m_duration = success ? duration : gint64(-1);
	}
	else
		m_duration = GST_CLOCK_TIME_NONE;
}


void pipeline::update_cur_pad_offset()
{
	if (!m_current_stream)
		return;

	if (m_current_stream->m_media.m_start_time != GST_CLOCK_TIME_NONE)
	{
		GstClockTime cur_time = get_current_time();
		GstClockTimeDiff time_diff = GST_CLOCK_DIFF(cur_time, m_current_stream->m_media.m_start_time);
		LOG_MSG(debug, "media is to be played in " << double(time_diff) / double(GST_SECOND) << " seconds");

		if (time_diff > (1 * GST_SECOND))
		{
			LOG_MSG(debug, "wait period until the media plays exceeds the threshold; adding pad offset");
			GstPad *sinkpad = gst_element_get_static_pad(m_current_stream->m_injecteos, "sink");
			gst_pad_set_offset(sinkpad, time_diff);
			gst_object_unref(GST_OBJECT(sinkpad));
		}
	}
	else
	{
		LOG_MSG(debug, "media is to be played now");
	}
}


void pipeline::create_dot_pipeline_dump(std::string const &p_extra_name)
{
	std::string filename = std::string("reeldaemon_curstate-") + get_state_name(m_current_state) + "_" + p_extra_name;
	GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS(GST_BIN(m_pipeline), GST_DEBUG_GRAPH_SHOW_ALL, filename.c_str());
}


void pipeline::set_pipeline_to_idle()
{
	m_current_state = state_stopping;
	GstStateChangeReturn ret = gst_element_set_state(GST_ELEMENT(m_pipeline), GST_STATE_NULL);
	if (ret != GST_STATE_CHANGE_ASYNC)
	{
		LOG_MSG(debug, "GStreamer state change to NULL completed immediately; can set to idle state right now");
		set_state_to_initial_value();
		m_current_state = state_idle;
		handle_postponed_requests();
	}
	else
		LOG_MSG(debug, "GStreamer state change to NULL is completing asynchronously");
}


void pipeline::set_state_to_initial_value()
{
	m_current_gstreamer_state = GST_STATE_NULL;
	m_pending_gstreamer_state = GST_STATE_VOID_PENDING;
	m_duration = -1;
}


bool pipeline::set_gstreamer_state(GstState const p_new_gstreamer_state)
{
	if (m_current_gstreamer_state == p_new_gstreamer_state)
		return true;

	LOG_MSG(debug, "switching state of GStreamer pipeline from " << gst_element_state_get_name(m_current_gstreamer_state) << " to " << gst_element_state_get_name(p_new_gstreamer_state));

	GstStateChangeReturn ret = gst_element_set_state(GST_ELEMENT(m_pipeline), p_new_gstreamer_state);
	LOG_MSG(debug, "return value after starting state change: " << gst_element_state_change_return_get_name(ret));

	switch (ret)
	{
		case GST_STATE_CHANGE_FAILURE:
			LOG_MSG(error, "switching GStreamer pipeline state to " << gst_element_state_get_name(p_new_gstreamer_state) << " failed");
			return false;
		default:
			return true;
	}
}


bool pipeline::is_transitioning() const
{
	switch (m_current_state)
	{
		case state_starting:
		case state_resetting:
		case state_stopping:
			return true;
		default:
			return (m_pending_gstreamer_state != GST_STATE_VOID_PENDING);
	}
}


void pipeline::handle_postponed_requests()
{
	if (m_postponed_stop)
	{
		m_postponed_stop = false;
		stop();
	}
	else if (is_valid(m_postponed_media))
	{
		media m = m_postponed_media;
		play_media(m, true);
	}
}


static void new_pad_callback(GstElement *, GstPad *p_pad, gpointer p_data)
{
	GstElement *inject_eos = GST_ELEMENT(p_data);

	GstCaps *caps;
	GstStructure *str;
	GstPad *sinkpad;

	sinkpad = gst_element_get_static_pad(inject_eos, "sink");
	auto elems_guard = make_scope_guard([&]() { checked_unref(sinkpad); });

	// only link once
	if (GST_PAD_IS_LINKED(sinkpad))
		return;

	// check media type
	caps = gst_pad_query_caps(p_pad, nullptr);
	str = gst_caps_get_structure(caps, 0);
	bool match = g_strrstr(gst_structure_get_name(str), "video");
	gst_caps_unref(caps);
	if (!match)
		return;

	gst_pad_link(p_pad, sinkpad);
}


stream_sptr pipeline::setup_stream(media const &p_media)
{
	stream_sptr new_stream(new stream(m_concat, GST_BIN(m_pipeline)));
	new_stream->m_media = p_media;
	new_stream->m_uridecodebin = gst_element_factory_make("uridecodebin", nullptr);
	new_stream->m_injecteos = gst_element_factory_make("injecteos", nullptr);

	auto elems_guard = make_scope_guard([&]()
	{
		checked_unref(new_stream->m_uridecodebin);
		checked_unref(new_stream->m_injecteos);
	});

	if (new_stream->m_uridecodebin == nullptr)
		throw std::runtime_error(std::string("could not create uridecodebin for stream ") + p_media.m_uri);

	if (new_stream->m_injecteos == nullptr)
		throw std::runtime_error(std::string("could not create injecteos element for stream ") + p_media.m_uri);

	gst_bin_add_many(GST_BIN(m_pipeline), new_stream->m_uridecodebin, new_stream->m_injecteos, nullptr);
	g_object_set(G_OBJECT(new_stream->m_uridecodebin), "uri", p_media.m_uri.c_str(), nullptr);

	g_object_set(G_OBJECT(new_stream->m_injecteos), "eos-time", p_media.m_stop_time, nullptr);

	GstPad *injecteos_srcpad = gst_element_get_static_pad(new_stream->m_injecteos, "src");

	GstPadTemplate *concat_sinkpad_template = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(m_concat), "sink_%u");
	GstPad *concat_sinkpad = gst_element_request_pad(m_concat, concat_sinkpad_template, nullptr, nullptr);
	gst_pad_link(injecteos_srcpad, concat_sinkpad);
	new_stream->m_concat_sinkpad = concat_sinkpad;

	gst_object_unref(GST_OBJECT(injecteos_srcpad));

	g_signal_connect(new_stream->m_uridecodebin, "pad-added", G_CALLBACK(new_pad_callback), gpointer(new_stream->m_injecteos));

	gst_element_sync_state_with_parent(new_stream->m_injecteos);
	gst_element_sync_state_with_parent(new_stream->m_uridecodebin);

	elems_guard.unguard();

	return new_stream;
}


void pipeline::static_about_to_switch_pad(GstElement *p_concat, gpointer p_user_data)
{
	static_cast < pipeline* > (p_user_data)->about_to_switch_pad(p_concat);
}


void pipeline::about_to_switch_pad(GstElement *)
{
	std::lock_guard < std::mutex > lock(m_mutex);

	if (m_current_stream)
		m_old_streams.push(m_current_stream);

	m_current_stream = m_next_stream;
	m_next_stream.reset();

	update_cur_pad_offset();

	GstBus *bus = gst_pipeline_get_bus(GST_PIPELINE(m_pipeline));
	gst_bus_post(
		bus,
		gst_message_new_application(
			GST_OBJECT(m_pipeline),
			gst_structure_new_empty("Switch")
		)
	);
	gst_object_unref(GST_OBJECT(bus));
}


gboolean pipeline::static_bus_watch(GstBus *p_bus, GstMessage *p_msg, gpointer p_user_data)
{
	return static_cast < pipeline* > (p_user_data)->bus_watch(p_bus, p_msg);
}


bool pipeline::bus_watch(GstBus *, GstMessage *p_msg)
{
	switch (GST_MESSAGE_TYPE(p_msg))
	{
		case GST_MESSAGE_APPLICATION:
		{
			if (!gst_message_has_name(p_msg, "Switch"))
				break;

			std::lock_guard < std::mutex > lock(m_mutex);

			while (!(m_old_streams.empty()))
			{
				stream_sptr old_stream = m_old_streams.front();
				m_old_streams.pop();
				old_stream.reset();
			}

			break;
		}

		case GST_MESSAGE_STATE_CHANGED:
		{
			GstState old_gstreamer_state, new_gstreamer_state, pending_gstreamer_state;
			gst_message_parse_state_changed(p_msg, &old_gstreamer_state, &new_gstreamer_state, &pending_gstreamer_state);

			// Only react once all of playbin switched state
			if (GST_MESSAGE_SRC(p_msg) != GST_OBJECT(m_pipeline))
				break;

			LOG_MSG(
				trace,
				"GStreamer state change:  "
				<< " old: " << gst_element_state_get_name(old_gstreamer_state)
				<< " new: " << gst_element_state_get_name(new_gstreamer_state)
				<< " pending: " << gst_element_state_get_name(pending_gstreamer_state)
			);

			std::string dotfilename = std::string("gststatechange_old-") + gst_element_state_get_name(old_gstreamer_state) + "_new-" + gst_element_state_get_name(new_gstreamer_state) + "_pending-" + gst_element_state_get_name(pending_gstreamer_state);

			m_current_gstreamer_state = new_gstreamer_state;

			create_dot_pipeline_dump(dotfilename);

			switch (m_current_state)
			{
				case state_starting:
				{
					switch (new_gstreamer_state)
					{
						case GST_STATE_READY:
							update_cur_pad_offset();
							break;

						case GST_STATE_PAUSED:
							update_duration();
							m_current_state = state_paused;
							break;

						case GST_STATE_PLAYING:
							m_current_state = state_playing;
							break;

						default:
							break;
					}

					break;
				}

				case state_stopping:
				{
					switch (new_gstreamer_state)
					{
						case GST_STATE_NULL:
							m_current_state = state_idle;
							set_state_to_initial_value();
							handle_postponed_requests();
							break;

						default:
							break;
					}
					break;
				}

				case state_playing:
				case state_paused:
				{
					switch (new_gstreamer_state)
					{
						case GST_STATE_PAUSED:
						case GST_STATE_PLAYING:
							handle_postponed_requests();
							break;

						default:
							break;
					}
					break;
				}

				default:
					break;
			}

			break;
		}

		case GST_MESSAGE_DURATION_CHANGED:
		{
			update_duration();
			break;
		}

		case GST_MESSAGE_INFO:
		{
			LOG_MSG(info, "info message received: " << message_to_str(p_msg, GST_LEVEL_INFO));
			break;
		}

		case GST_MESSAGE_WARNING:
		{
			LOG_MSG(warning, "warning message received: " << message_to_str(p_msg, GST_LEVEL_WARNING));
			break;
		}

		case GST_MESSAGE_ERROR:
		{
			LOG_MSG(error, "error message received: " << message_to_str(p_msg, GST_LEVEL_ERROR));
//			emit_error();
			break;
		}

		case GST_MESSAGE_EOS:
		{
			stop();
			break;
		}


		default:
			break;
	}

	return true;
}


} // namespace reeldaemon end
