#include <gst/gst.h>
#include <glib-unix.h>
#include <stdexcept>
#include <memory>
#include <time.h>
#include <cstring>
#include <locale>
#include <iostream>

#include "log.hpp"
#include "static-plugin.h"
#include "system.hpp"


int main(int argc, char *argv[])
{
	// Parse the commandline arguments
	// *not* printing errors with LOG_MSG here, since the logger isn't set up yet

	gint port = 55676;
	gboolean interactive = FALSE;
	gchar *uri_cstr = nullptr, *next_uri_cstr = nullptr;
	gchar *log_level_cstr = nullptr;
	static GOptionEntry const option_entries[] =
	{
		{ "port", 'p', 0, G_OPTION_ARG_INT, &port, "Port for TCP listening socket", "PORT" },
		{ "uri", 0, 0, G_OPTION_ARG_STRING, &uri_cstr, "URI to play", "URI" },
		{ "next-uri", 0, 0, G_OPTION_ARG_STRING, &next_uri_cstr, "Next URI to play", "URI" },
		{ "interactive", 'i', 0, G_OPTION_ARG_NONE, &interactive, "", nullptr },
		{ "log-level", 0, 0, G_OPTION_ARG_STRING, &log_level_cstr, "Log level (valid values: error warning info debug trace)", nullptr },
		{ nullptr, 0, 0, G_OPTION_ARG_NONE, nullptr, nullptr, nullptr }
	};

	GError *err = NULL;
	GOptionContext *argparse_ctx = g_option_context_new("- run playback reel daemon");
	g_option_context_add_main_entries(argparse_ctx, option_entries, nullptr);
	if (!g_option_context_parse(argparse_ctx, &argc, &argv, &err))
	{
		g_assert(err != NULL);
		std::cerr << "option parsing failed: " << err->message << std::endl;
		g_error_free(err);
		return 1;
	}

	if ((port < 1) || (port > 65535))
	{
		LOG_MSG(error, "invalid port " << port);
		return 1;
	}


	// Set up the logger

	{
		reeldaemon::log_levels log_level = reeldaemon::log_level_error;

		if (log_level_cstr != nullptr)
		{
			if (g_strcmp0(log_level_cstr, "trace") == 0)
				log_level = reeldaemon::log_level_trace;
			else if (g_strcmp0(log_level_cstr, "debug") == 0)
				log_level = reeldaemon::log_level_debug;
			else if (g_strcmp0(log_level_cstr, "info") == 0)
				log_level = reeldaemon::log_level_info;
			else if (g_strcmp0(log_level_cstr, "warning") == 0)
				log_level = reeldaemon::log_level_warning;
			else if (g_strcmp0(log_level_cstr, "error") == 0)
				log_level = reeldaemon::log_level_error;

			g_free(log_level_cstr);
		}

		reeldaemon::set_min_log_level(log_level);
	}


	// Copy URI strings to std::string

	std::string uri, next_uri;
	if (uri_cstr != nullptr)
	{
		uri = uri_cstr;
		g_free(uri_cstr);
	}
	if (next_uri_cstr != nullptr)
	{
		next_uri = next_uri_cstr;
		g_free(next_uri_cstr);
	}


	// Set up GStreamer

	if (!gst_init_check(&argc, &argv, &err))
	{
		LOG_MSG(error, "initializing GStreamer failed: " << err->message);
		return 1;
	}

	// Register the extra static elements (inject-eos and the copy of the concat element)
	register_static_elements();


	try
	{
		// Set up and start main loop

		reeldaemon::system sys(interactive, port);
		if (!(uri.empty()))
			sys.play(uri);
		if (!(next_uri.empty()))
			sys.set_next(next_uri);

		sys.run();
	}
	catch (std::runtime_error const &exc)
	{
		LOG_MSG(error, exc.what());
	}


	// Cleanup

	LOG_MSG(debug, "quitting");
	g_option_context_free(argparse_ctx);
	return 0;
}
