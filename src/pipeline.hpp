#ifndef REELDAEMON_PIPELINE_HPP
#define REELDAEMON_PIPELINE_HPP

#include <functional>
#include <memory>
#include <string>
#include <mutex>
#include <queue>
#include <gst/gst.h>
#include "media.hpp"


namespace reeldaemon
{


enum state
{
	state_idle,
	state_starting,
	state_resetting,
	state_stopping,
	state_playing,
	state_paused
};


std::string get_state_name(state const p_state);


struct stream
{
	media m_media;
	GstElement *m_uridecodebin, *m_injecteos;
	GstPad *m_concat_sinkpad;

	stream();
	explicit stream(GstElement *p_concat, GstBin *p_bin);
	~stream();

private:
	GstElement *m_concat;
	GstBin *m_bin;
};

typedef std::shared_ptr < stream > stream_sptr;

bool is_valid(stream const &p_stream);


class pipeline
{
public:
	pipeline();
	~pipeline();

	bool play_media(media const &p_media, bool const p_play_now);
	void stop();
	gint64 get_current_position() const;
	GstClockTime get_current_time() const;
	GstClockTime get_duration() const;

	pipeline(pipeline const &) = delete;
	pipeline(pipeline &&) = delete;
	pipeline& operator = (pipeline const &) = delete;
	pipeline& operator = (pipeline &&) = delete;


private:
	void update_duration();
	void update_cur_pad_offset();
	void create_dot_pipeline_dump(std::string const &p_extra_name);
	void set_pipeline_to_idle();
	void set_state_to_initial_value();
	bool set_gstreamer_state(GstState const p_new_gstreamer_state);
	bool is_transitioning() const;
	void handle_postponed_requests();
	stream_sptr setup_stream(media const &p_media);


	// C callbacks and their C++ counterparts

	static void static_about_to_switch_pad(GstElement *p_concat, gpointer p_user_data);
	void about_to_switch_pad(GstElement *p_concat);

	static gboolean static_bus_watch(GstBus *p_bus, GstMessage *p_msg, gpointer p_user_data);
	bool bus_watch(GstBus *p_bus, GstMessage *p_msg);


	state m_current_state;
	GstState m_current_gstreamer_state, m_pending_gstreamer_state;
	GstElement *m_pipeline, *m_concat, *m_video_sink;
	GstClock *m_clock;
	gint64 m_duration;
	std::mutex m_mutex;

	bool m_postponed_stop;
	media m_postponed_media;

	typedef std::queue < stream_sptr > stream_queue;
	stream_sptr m_current_stream, m_next_stream;
	stream_queue m_old_streams;
};


} // namespace reeldaemon end


#endif
