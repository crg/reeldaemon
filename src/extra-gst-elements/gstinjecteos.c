#include "gstinjecteos.h"


GST_DEBUG_CATEGORY_STATIC(injecteos_debug);
#define GST_CAT_DEFAULT injecteos_debug


enum
{
	PROP_0,
	PROP_EOS_TIME
};


#define DEFAULT_EOS_TIME GST_CLOCK_TIME_NONE


static GstStaticPadTemplate static_sink_template = GST_STATIC_PAD_TEMPLATE(
	"sink",
	GST_PAD_SINK,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS_ANY
);
static GstStaticPadTemplate static_src_template = GST_STATIC_PAD_TEMPLATE(
	"src",
	GST_PAD_SRC,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS_ANY
);


G_DEFINE_TYPE(GstInjectEOS, gst_inject_eos, GST_TYPE_BASE_TRANSFORM)


static void gst_inject_eos_set_property(GObject *object, guint prop_id, GValue const *value, GParamSpec *pspec);
static void gst_inject_eos_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec);
static gboolean gst_inject_eos_sink_event(GstBaseTransform *trans, GstEvent *event);
static GstFlowReturn gst_inject_eos_transform_ip(GstBaseTransform *trans, GstBuffer *buf);




static void gst_inject_eos_class_init(GstInjectEOSClass *klass)
{
	GObjectClass *object_class;
	GstElementClass *element_class;
	GstBaseTransformClass *basetransform_class;

	GST_DEBUG_CATEGORY_INIT(injecteos_debug, "injecteos", 0, "EOS event injector");

	object_class = G_OBJECT_CLASS(klass);
	element_class = GST_ELEMENT_CLASS(klass);
	basetransform_class = GST_BASE_TRANSFORM_CLASS(klass);

	gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&static_sink_template));
	gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&static_src_template));

	object_class->set_property          = GST_DEBUG_FUNCPTR(gst_inject_eos_set_property);
	object_class->get_property          = GST_DEBUG_FUNCPTR(gst_inject_eos_get_property);
	basetransform_class->sink_event     = GST_DEBUG_FUNCPTR(gst_inject_eos_sink_event);
	basetransform_class->transform_ip   = GST_DEBUG_FUNCPTR(gst_inject_eos_transform_ip);

	g_object_class_install_property(
	        object_class,
	        PROP_EOS_TIME,
	        g_param_spec_uint64(
	                "eos-time",
	                "EOS timestamp",
	                "When to inject an EOS event (in clock time, NOT running time)",
	                0, G_MAXUINT64,
			DEFAULT_EOS_TIME,
	                G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
	        )
	);

	gst_element_class_set_static_metadata(
		element_class,
		"EOS event injection element",
		"Generic",
		"inject an EOS event into a playing stream",
		"Carlos Rafael Giani <dv@pseudoterminal.org>"
	);
}


static void gst_inject_eos_init(GstInjectEOS *inject_eos)
{
	inject_eos->eos_time = DEFAULT_EOS_TIME;
}


static void gst_inject_eos_set_property(GObject *object, guint prop_id, GValue const *value, GParamSpec *pspec)
{
	GstInjectEOS *inject_eos = GST_INJECT_EOS(object);
	switch (prop_id)
	{
		case PROP_EOS_TIME:
		{
			inject_eos->eos_time = g_value_get_uint64(value);
			break;
		}

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}


static void gst_inject_eos_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	GstInjectEOS *inject_eos = GST_INJECT_EOS(object);
	switch (prop_id)
	{
		case PROP_EOS_TIME:
		{
			g_value_set_uint64(value, inject_eos->eos_time);
			break;
		}

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}


static gboolean gst_inject_eos_sink_event(GstBaseTransform *trans, GstEvent *event)
{
	GstInjectEOS *inject_eos = GST_INJECT_EOS(trans);
	switch (GST_EVENT_TYPE(event))
	{
		case GST_EVENT_SEGMENT:
		{
			GstSegment const *seg = NULL;
			gst_event_parse_segment(event, &seg);
			inject_eos->segment = *seg;
			break;
		}

		default:
			break;
	}

	return GST_BASE_TRANSFORM_CLASS(gst_inject_eos_parent_class)->sink_event(trans, event);
}


static GstFlowReturn gst_inject_eos_transform_ip(GstBaseTransform *trans, GstBuffer *buf)
{
	GstClockTime buffer_running_time, base_time;
	GstInjectEOS *inject_eos = GST_INJECT_EOS_CAST(trans);

	base_time = gst_element_get_base_time(GST_ELEMENT_CAST(trans));
	buffer_running_time = gst_segment_to_running_time(&(inject_eos->segment), GST_FORMAT_TIME, GST_BUFFER_PTS(buf));

	if ((buffer_running_time != GST_CLOCK_TIME_NONE) && (inject_eos->eos_time != GST_CLOCK_TIME_NONE))
	{
		gboolean send_eos = FALSE;

		if (inject_eos->eos_time >= base_time)
		{
			GstClockTime eos_running_time = inject_eos->eos_time - base_time;

			GST_LOG_OBJECT(trans,
				"base time:  (%" G_GUINT64_FORMAT " (%" GST_TIME_FORMAT ")"
				"  running times:"
				"  buffer: %" GST_TIME_FORMAT
				"  eos: %" GST_TIME_FORMAT,
				base_time, GST_TIME_ARGS(base_time),
				GST_TIME_ARGS(buffer_running_time),
				GST_TIME_ARGS(eos_running_time));
			if (buffer_running_time >= eos_running_time)
				send_eos = TRUE;
		}
		else
		{
			GST_TRACE_OBJECT(
				trans,
				"eos time lies in the past"
				" (base time: (%" G_GUINT64_FORMAT " (%" GST_TIME_FORMAT
				" eos time: (%" G_GUINT64_FORMAT " (%" GST_TIME_FORMAT ")"
				" - not sending out EOS",
				base_time, GST_TIME_ARGS(base_time),
				inject_eos->eos_time, GST_TIME_ARGS(inject_eos->eos_time)
			);
		}

		if (send_eos)
		{
			GST_TRACE_OBJECT(trans, "sending EOS");
#if 1
			gst_pad_push_event(GST_BASE_TRANSFORM_SRC_PAD(trans), gst_event_new_eos());
#else
			gst_element_post_message(
				GST_ELEMENT_CAST(trans),
				gst_message_new_element(
					GST_OBJECT_CAST(trans),
					gst_structure_new_empty("customeos")
				)
			);
#endif
			return GST_FLOW_OK;
		}
	}
	else if (buffer_running_time == GST_CLOCK_TIME_NONE)
	{
		GST_LOG_OBJECT(trans, "buffer has no PTS");
	}
	else if (inject_eos->eos_time == GST_CLOCK_TIME_NONE)
	{
		GST_LOG_OBJECT(trans, "no eos time set");
	}

	return GST_FLOW_OK;
}
