#ifndef GSTINJECTEOS_H___
#define GSTINJECTEOS_H___

#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>


G_BEGIN_DECLS


typedef struct _GstInjectEOS GstInjectEOS;
typedef struct _GstInjectEOSClass GstInjectEOSClass;


#define GST_TYPE_INJECT_EOS             (gst_inject_eos_get_type())
#define GST_INJECT_EOS(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_INJECT_EOS, GstInjectEOS))
#define GST_INJECT_EOS_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_INJECT_EOS,  GstInjectEOSClass))
#define GST_INJECT_EOS_CAST(obj)        ((GstInjectEOS *)(obj))
#define GST_IS_INJECT_EOS(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_INJECT_EOS))
#define GST_IS_INJECT_EOS_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_INJECT_EOS))


struct _GstInjectEOS
{
	GstBaseTransform parent;
	GstClockTime eos_time;
	GstSegment segment;
};


struct _GstInjectEOSClass
{
	GstBaseTransformClass parent_class;
};


GType gst_inject_eos_get_type(void);


G_END_DECLS


#endif
