#include "gstconcat.h"
#include "gstinjecteos.h"


static gboolean plugin_init(GstPlugin *plugin)
{
	gboolean ret = TRUE;
	ret = ret && gst_element_register(plugin, "concat2", GST_RANK_NONE, gst_concat_get_type());
	ret = ret && gst_element_register(plugin, "injecteos", GST_RANK_NONE, gst_inject_eos_get_type());
	return ret;
}


void register_static_elements(void)
{
	gst_plugin_register_static(
		GST_VERSION_MAJOR,
		GST_VERSION_MINOR,
		"internal plugins",
		"reeldaemon internal static plugins",
		plugin_init,
		"1.0",
		"BSD",
		"reeldaemon-internal",
		"Unknown package release",
		"Unknown package origin"
	);
}
