#include <glib-unix.h>
#include <cstring>
#include <ctime>
#include "system.hpp"
#include "scope_guard.hpp"


namespace reeldaemon
{


namespace
{


gboolean sigint_handler(gpointer p_ptr)
{
	g_main_loop_quit(static_cast < GMainLoop* > (p_ptr));
	return TRUE;
}


std::string validate_uri(std::string const &p_uri)
{
	if (gst_uri_is_valid(p_uri.c_str()))
	{
		return p_uri;
	}
	else
	{
		gchar *uri_cstr = gst_filename_to_uri(p_uri.c_str(), nullptr);
		if (uri_cstr != nullptr)
		{
			std::string uri;
			uri = uri_cstr;
			g_free(uri_cstr);
			return uri;
		}
		else
			return "";
	}
}


GstClockTime parse_time_string(pipeline &p_pipeline, std::string p_time_string)
{
	if (p_time_string.empty())
		return GST_CLOCK_TIME_NONE;

	std::tm tm;
	bool is_relative = (p_time_string[0] == '+');
	char const *actual_time_string = p_time_string.c_str() + (is_relative ? 1 : 0);
	auto get_time_offset = [&]() { return is_relative ? p_pipeline.get_current_time() : 0; };

	// Full calendar timestamp. YYYY-MM-DD HH:MM:ss
	std::memset(&tm, 0, sizeof(tm));
	if (strptime(actual_time_string, "%Y-%m-%d %H:%M:%S", &tm) != NULL)
	{
		std::time_t t = std::mktime(&tm);
		LOG_MSG(debug, "time string: " << p_time_string << "  seconds since epoch: " << t);
		return GST_SECOND * t + get_time_offset();
	}

	// Simple timespan. HH:MM:ss. Current date is implicitely added if it is an absolute time.
	std::memset(&tm, 0, sizeof(tm));
	if (strptime(actual_time_string, "%H:%M:%S", &tm) != NULL)
	{
		if (is_relative)
		{
			// Time string is relative, so assume day, month, year are all zero.
			GstClockTime t = GST_SECOND * (tm.tm_sec + tm.tm_min * 60 + tm.tm_hour * 3600);
			LOG_MSG(debug, "time string: " << p_time_string << "  nanoseconds: " << t);
			return t + p_pipeline.get_current_time();
		}
		else
		{
			// Time string is absolute. Assume it implies the current date.

			// Get the current date as a tm struct
			std::time_t t_cur;
			time(&t_cur);
			struct tm *tm_cur = localtime(&t_cur);

			// Make a copy of the previously parsed tm struct (containing hours, minutes, seconds)
			struct tm tm_copy = tm;
			// Take the values for the current time, but use the hours/minutes/seconds of the
			// copy that was made earlier, thus combining the current date with the specified time
			tm = *tm_cur;
			tm.tm_hour = tm_copy.tm_hour;
			tm.tm_min = tm_copy.tm_min;
			tm.tm_sec = tm_copy.tm_sec;

			std::time_t t = std::mktime(&tm);
			LOG_MSG(debug, "time string: " << p_time_string << "  seconds since epoch: " << t);
			return GST_SECOND * t;
		}
	}

	// If everything else fails, try if this is just a nanosecond timestamp
	try
	{
		gint64 t = std::stoll(std::string(p_time_string));
		LOG_MSG(debug, "nanoseconds: " << t);
		return (t < 0) ? GST_CLOCK_TIME_NONE : (t + get_time_offset());
	}
	catch (std::logic_error const &)
	{
	}

	LOG_MSG(error, "could not parse time string \"" << p_time_string << "\"");
	return GST_CLOCK_TIME_NONE;
}


} // unnamded namespace end




system::system(bool const p_interactive_mode, int const p_port)
	: m_interactive_mode(p_interactive_mode)
	, m_loop(nullptr)
	, m_tcp_listener(nullptr)
	, m_cmdline_iface(nullptr)
{
	// Set up GLib mainloop
	m_loop = g_main_loop_new(nullptr, TRUE);

	// Make use of RAII to ensure the mainloop is unref'd if an error happens
	auto mainloop_guard = make_scope_guard([&]()
	{
		if (m_loop != nullptr)
			g_main_loop_unref(m_loop);
	});

	// Install SIGINT handler to catch Ctrl+C keypresses
	g_unix_signal_add(SIGINT, sigint_handler, m_loop);


	// Set up TCP listening socket and command line interface (if enabled)
	// the line_cb is passed to both, making it possible to process commands
	// coming from either one of them

	auto line_cb = [&](std::string const &p_line) { return process_command(tokenize_line(p_line)); };

	if (p_interactive_mode)
	{
		m_cmdline_iface = cmdline_iface_uptr(new cmdline_iface("cmd> ", line_cb));
		LOG_MSG(debug, "interactive mode enabled; initialized command console");
	}

	m_tcp_listener = tcp_listener_uptr(new tcp_listener(p_port, line_cb));


	// Set up the GStreamer playback pipeline

	m_pipeline = pipeline_uptr(new pipeline());


	// We are done; the mainloop_guard no longer has to watch the mainloop
	mainloop_guard.unguard();

	LOG_MSG(info, "reeldaemon started");
}


system::~system()
{
	if (m_loop != nullptr)
		g_main_loop_unref(m_loop);
}


void system::run()
{
	g_assert(m_loop != nullptr);
	g_main_loop_run(m_loop);
}


void system::play(std::string const &p_uri)
{
	m_pipeline->play_media(media(validate_uri(p_uri), GST_CLOCK_TIME_NONE), true);
}


void system::set_next(std::string const &p_uri)
{
	m_pipeline->play_media(media(validate_uri(p_uri), GST_CLOCK_TIME_NONE), false);
}


bool system::process_command(tokens p_tokens)
{
	if (p_tokens.empty())
		return true;

	auto command = p_tokens[0];

	if (command == "quit")
	{
		g_main_loop_quit(m_loop);
		return false;
	}
	else if ((command == "play") || (command == "setnext"))
	{
		if (p_tokens.size() < 2)
		{
			LOG_MSG(error, "incorrect " << command << " command. usage: " << command << " <URI> [<start time>] [<stop time>]");
		}
		else
		{
			GstClockTime start_time = (p_tokens.size() > 2) ? parse_time_string(*m_pipeline, p_tokens[2]) : GST_CLOCK_TIME_NONE;
			GstClockTime stop_time = (p_tokens.size() > 3) ? parse_time_string(*m_pipeline, p_tokens[3]) : GST_CLOCK_TIME_NONE;
			m_pipeline->play_media(media(validate_uri(p_tokens[1]), start_time, stop_time), (command == "play"));
		}
	}
	else if (command == "curtime")
	{
		GstClockTime cur_time = m_pipeline->get_current_time();
		std::time_t t = std::time_t(cur_time / GST_SECOND);
		std::tm *tm = std::localtime(&t);
		char buf[400];
		strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", tm);
		LOG_MSG(info, "user requested current time: " << cur_time << " (" << buf << ")");
	}

	return true;
}


} // namespace reeldaemon end
