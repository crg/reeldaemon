#include "media.hpp"


namespace reeldaemon
{


media::media()
	: m_start_time(GST_CLOCK_TIME_NONE)
	, m_stop_time(GST_CLOCK_TIME_NONE)
{
}


media::media(std::string const &p_uri, GstClockTime const p_start_time, GstClockTime const p_stop_time)
	: m_uri(p_uri)
	, m_start_time(p_start_time)
	, m_stop_time(p_stop_time)
{
}


bool is_valid(media const &p_media)
{
	return !(p_media.m_uri.empty());
}


} // namespace reeldaemon end
