#ifndef REELDAEMON_MEDIA_HPP
#define REELDAEMON_MEDIA_HPP

#include <string>
#include <gst/gst.h>


namespace reeldaemon
{


struct media
{
	std::string m_uri;
	GstClockTime m_start_time;
	GstClockTime m_stop_time;

	media();
	explicit media(std::string const &p_uri, GstClockTime const p_start_time, GstClockTime const p_stop_time = GST_CLOCK_TIME_NONE);
};


bool is_valid(media const &p_media);


} // namespace reeldaemon end


#endif
