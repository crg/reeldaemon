reeldaemon
==========

About
-----

This is a GStreamer-based media player in form of a Unix daemon. It can also be run in
an interactive readline-based command-line mode, however.


License
-------

This code is licensed under the GPL v2. See the accompanying LICENSE file for details.


Dependencies
------------

* gcc 4.8 or newer (for proper C++11 support)
* GStreamer 1.4.5 or newer (other 1.4.x versions may work, but are untested; the 1.2.x series has bugs with pad offsets)
* libreadline 6.3 or newer

Make sure the headers for libreadline, gstreamer, and glib are available. On Debian/Ubuntu, install these packages:

    libgstreamer1.0-dev libglib2.0-dev libreadline-dev


Building and installing
-----------------------

This project uses the [waf meta build system](https://code.google.com/p/waf/). To configure , first set
the following environment variables to whatever is necessary for cross compilation for your platform:

* `CC`
* `CFLAGS`
* `LDFLAGS`
* `PKG_CONFIG_PATH`
* `PKG_CONFIG_SYSROOT_DIR`

Then, run:

    ./waf configure --prefix=PREFIX

(The aforementioned environment variables are only necessary for this configure call.)
PREFIX defines the installation prefix, that is, where the built binaries will be installed.

Once configuration is complete, run:

    ./waf

This builds the reeldaemon binary.
Finally, to install, run:

    ./waf install


Running reeldaemon
------------------

reeldaemon --help returns:

    Usage:
      reeldaemon [OPTION...] - run playback reel daemon
    
    Help Options:
      -h, --help            Show help options
    
    Application Options:
      -p, --port=PORT       Port for TCP listening socket
      --uri=URI             URI to play
      --next-uri=URI        Next URI to play
      -i, --interactive
      --log-level           Log level (valid values: error warning info debug trace)

reeldaemon opens a TCP listening socket, and waits for incoming command lines through there. It is also possible
enable the interactive command console by passing the -i flag.

Through the TCP socket, it is possible to send command lines over a TCP connection. Example:

    echo "play /tmp/test.mkv \"+00:00:05\"" | nc localhost 55666

(This assumes the reeldaemon is running on the same host, and listens to port 55666.)


Command lines
-------------

The command lines follow conventions similar to those in bash. In particular, it allows for grouping
parts to one token by using quotes or double quotes, and escape quotes. Example:

    hello "world abc" "def\"ghi" 'jkl " xxx' 'u\'v'

is tokenized to (brackets added here only for clarity):

    [hello] [world abc] [def"ghi] [jkl " xxx] [u'v]

This is useful for URIs and/or filenames with whitespaces, quotes etc.

Here is a list of command lines:

* `play <URI or absolute filename> [<optional start time>]`
  Starts playback of the given URI/file immediately. If a start time is given, it plays at this given time,
  and waits if this start time is in the future. If the start time is relative, it will be added to the
  current time, and this is the start time.
  Note that if media is currently playing, and `setnext` was called earlier to queue a new video, this queued video is cleared. In other words, `play` undoes the effect of `setnext`.

* `setnext <URI or absolute filename> [<optional start time>]`
Works just like `play`, except that it queues the given URI/file if another one is currently playing. Otherwise, it starts playback of the given URI/file immediately, just like `play`.
If a start time is given, and it lies within the duration of the currently playing video, the current one will be stopped at that time, and the next one will start playing.

* `curtime`
Returns the current time since the Unix Epoch, in nanoseconds.

* `quit`
Quits the reeldaemon.

Examples:

* `play file:///mnt/videos/test1.mkv "2015-03-20 15:15:00"` starts playback of `test1.mkv` at 15:15 on March 20th 2015
* `play file:///mnt/videos/test1.mkv "2015-03-20 15:15:00 15:17:00"` starts playback of `test1.mkv` at 15:15 on March 20th 2015, and stops 2 minutes later, at 15:17
* `play file:///mnt/videos/test2.mkv` starts playback of `test2.mkv` immediately
* `play file:///mnt/videos/test3.mkv "+00:00:11"` starts playback of `test3.mkv` in 11 seconds
* `setnext file:///mnt/videos/test3.mkv "+00:00:21"` starts playback of `test3.mkv` in 21 seconds ; if another file is currently playing at that time, it will be stopped, and test3.mkv will start playing
* `setnext file:///mnt/videos/test3.mkv "+00:00:05" "+00:00:20"` starts playback of `test3.mkv` in 5 seconds , and stops in 20 seconds ; if another file is currently playing at that time, it will be stopped, and test3.mkv will start playing
* `setnext file:///mnt/videos/test4.mkv "1923152947916260009"` starts playback of `test4.mkv` at Unix timestamp 1923152947916260009
* `setnext file:///mnt/videos/test4.mkv "+5000000000"` starts playback of `test4.mkv` in 5 seconds


Start/stop time value format
----------------------------

Time values can be absolute or relative. Relative values begin with a plus character. When encountering these, the
reeldaemon queries the current time, and adds this time value to it. Example:

    +00:31:15

This time value lies 31 minutes and 15 seconds in the future.

Time values are specified as either a ISO 8601 compatible format (more human-readable), or a Unix timestamp (in nanoseconds).
The former is a string with the given format:

    YYYY-MM-DD HH:MM:ss

YYYY = full year, MM the month (01-12), DD the day of the month (01-31), HH the hour (00-23), MM minutes (00-59), ss seconds (00-59).

Example: `2015-03-21 09:10:05`

(NOTE: be sure to use quotes when using such a compatible string, because of the whitespace between date and time!)

The latter is a 64-bit integer, with 0 being the Unix epoch (January 1st, 1970). Internally, the reeldaemon - and in fact GStreamer itself - uses 64-bit timestamps for time measurement and specification.

In addition, a shortened version of the ISO 8601 compatible format is also possible:

    HH:MM:ss

That is, the time without the date. This is useful for relative values, which are explained below.
If the value is relative, then year, month, and date are assumed to be zero. If the value is absolute, they are assumed to be the current date.
